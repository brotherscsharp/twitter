import axios from '../axios.config';
import { IGetTwitProtosResponse } from '../models/responses/IGetTwitProtosResponse';
import { IGetTwitResponse } from '../models/responses/IGetTwitResponse';
import { IGetTwitTimeResponse } from '../models/responses/IGetTwitTimeResponse';
import { ITwitResponse } from '../models/responses/ITwitResponse';
import { ITwitProtos } from '../models/common/ITwitProtos';
import { ITwitTime } from '../models/common/ITwitTime';

export const getTwits = async (page = 1): Promise<ITwitResponse[]> => {
  try {
    const { data } = await axios.get<IGetTwitResponse>(`tweets?page=${page}`);
    return data.data;
  } catch (e) {
    return [];
  }
};

export const getTweetPhoto = async (tweetId: string): Promise<ITwitProtos[]> => {
  try {
    const { data } = await axios.get<IGetTwitProtosResponse>(`tweets/${tweetId}/photos`);
    return data.data;
  } catch (e) {
    return [];
  }
};

export const getTweetTime = async (tweetId: string): Promise<ITwitTime[]> => {
  try {
    const { data } = await axios.get<IGetTwitTimeResponse>(`tweets/${tweetId}/time`);
    return data.data;
  } catch (e) {
    return [];
  }
};
