import React, { ReactElement } from 'react';
import { TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { VStack } from '@gluestack-ui/themed';
import { useNavigation } from '@react-navigation/native';
import { Card } from '../Card';
import { Like } from '../../resources/svgs';
import { setLikedTwits } from '../../store/reducers/CommonReducer';
import { RootState } from '../../store';
import { StackNavigation } from '../../App';
import { ITwitResponse } from '../../models/responses/ITwitResponse';
import styles from './index.style';

type TwitCardProps = {
  children: ReactElement;
  twit: ITwitResponse;
};

export const TwitCard = ({ children, twit }: TwitCardProps) => {
  const dispatch = useDispatch();
  const { navigate } = useNavigation<StackNavigation>();
  const likedTwits: string[] = useSelector((state: RootState) => state.common.likedTwits);

  const onTwitPressed = () => {
    navigate('Details', { twit });
  };

  const onLikePressed = () => {
    dispatch(setLikedTwits(twit.Id));
  };

  const isLiked = () => {
    return likedTwits.includes(twit?.Id);
  };

  return (
    <Card>
      <VStack>
        <TouchableWithoutFeedback onPress={onTwitPressed}>{children}</TouchableWithoutFeedback>

        <TouchableOpacity onPress={onLikePressed}>
          <Like alt="like" style={styles.like} fill={isLiked() ? 'red' : 'black'} />
        </TouchableOpacity>
      </VStack>
    </Card>
  );
};
