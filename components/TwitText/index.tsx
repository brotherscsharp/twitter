import React from 'react';
import { VStack } from '@gluestack-ui/themed';
import { Dimensions } from 'react-native';
import RenderHtml from 'react-native-render-html';
import { TwitCard } from '../TwitCard';
import { TwitProps } from '../TwitPhoto';

const { width } = Dimensions.get('screen');

export const TwitText = ({ twit }: TwitProps) => {
  const getTrimmedComment = () => (twit.Comment.length > 300 ? `${twit.Comment.substring(0, 300)}...` : twit.Comment);

  return (
    <TwitCard twit={twit}>
      <VStack>
        <RenderHtml
          contentWidth={width}
          source={{
            html: getTrimmedComment(),
          }}
        />
      </VStack>
    </TwitCard>
  );
};
