import React, { ReactElement } from 'react';
import { View } from '@gluestack-ui/themed';
import styles from './index.style';

type CardProps = {
  children: ReactElement;
};

export const Card = ({ children }: CardProps) => {
  return (
    <View style={styles.container}>
      <View style={[styles.card, styles.elevation]}>{children}</View>
    </View>
  );
};
