import { Dimensions, StyleSheet } from 'react-native';

const { width } = Dimensions.get('screen');
export default StyleSheet.create({
  stackContainer: {
    alignItems: 'center',
  },
  imageContainer: {
    width,
    height: 700,
    alignItems: 'center',
  },
  image: {
    flex: 0.4,
    width: '80%',
  },
  carouselContainer: {
    flex: 0.5,
    height: 290,
  },
});
