import React, { useEffect, useState } from 'react';
import { View, Image, VStack } from '@gluestack-ui/themed';
import { Dimensions } from 'react-native';
import { ITwitProtos } from '../../models/common/ITwitProtos';
import { ITwitResponse } from '../../models/responses/ITwitResponse';
import { getTweetPhoto } from '../../services/apiService';
import { TwitCard } from '../TwitCard';
import Carousel from 'react-native-reanimated-carousel';
import styles from './index.style';

const { width } = Dimensions.get('screen');
export type TwitProps = {
  twit: ITwitResponse;
};

export const TwitPhoto = ({ twit }: TwitProps) => {
  const [twits, setTweets] = useState<ITwitProtos[]>([]);

  const initPage = async () => {
    if (twit?.Id) {
      const twitsRes = await getTweetPhoto(twit.Id);
      setTweets(twitsRes);
    }
  };

  useEffect(() => {
    initPage();
  }, [twit?.Id]);

  return (
    <TwitCard twit={twit}>
      <VStack style={styles.stackContainer}>
        <Carousel
          width={width}
          height={width / 2 + 20}
          data={twits}
          scrollAnimationDuration={1000}
          renderItem={(item: ITwitProtos) => (
            <View key={item.Id} style={styles.imageContainer}>
              <Image
                source={
                  item.UrlOriginal?.startsWith('http')
                    ? { uri: item.UrlOriginal }
                    : require('../../assets/default-image.png')
                }
                resizeMode="cover"
                style={styles.image}
                alt={`Image ${item.UrlOriginal}`}
              />

              {/* 
              defaultSource does not work properly on Android. Crashes app

              <Image
                source={{ uri: item.UrlOriginal }}
                defaultSource={require('../../assets/default-image.png')}
                resizeMode="cover"
                style={styles.image}
                alt={`Image ${item.UrlOriginal}`}
              /> */}
            </View>
          )}
        />
      </VStack>
    </TwitCard>
  );
};
