import React from 'react';
import { VStack } from '@gluestack-ui/themed';
import { Video } from 'expo-av';
import { TwitCard } from '../TwitCard';
import styles from './index.style';
import { TwitProps } from '../TwitPhoto';

export const TwitVideo = ({ twit }: TwitProps) => {
  const defaultVideoUrl = 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4';

  const getVideoUrl = () => {
    return twit.VideoUrl.startsWith('http') ? twit.VideoUrl : defaultVideoUrl;
  };

  return (
    <TwitCard twit={twit}>
      <VStack>
        <Video source={{ uri: getVideoUrl() }} useNativeControls style={styles.player} />
      </VStack>
    </TwitCard>
  );
};
