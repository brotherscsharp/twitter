import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  timePeriod: {
    fontSize: 12,
  },
  divider: {
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
    marginVertical: 5,
  },
  timeTotal: {
    color: '#5a5a5a',
  },
});
