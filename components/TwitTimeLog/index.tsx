import React, { useEffect, useState } from 'react';
import { Text, VStack, View } from '@gluestack-ui/themed';
import { ITwitTime } from '../../models/common/ITwitTime';
import { getTweetTime } from '../../services/apiService';
import { calculateTotalTime, convertMiliseconds, formatDateRange, formatTotalDate } from '../../helpers';
import { TwitCard } from '../TwitCard';

import styles from './index.style';
import { TwitProps } from '../TwitPhoto';

export const TwitTimeLog = ({ twit }: TwitProps) => {
  const [twits, setTweets] = useState<ITwitTime[]>([]);
  const [formattedDate, setFormattedDate] = useState<string>('');

  const initPage = async () => {
    if (twit?.Id) {
      const twitsRes = await getTweetTime(twit.Id);
      const milliseconds = calculateTotalTime(twitsRes);
      const dates = convertMiliseconds(milliseconds);
      setTweets(twitsRes);
      setFormattedDate(formatTotalDate(dates));
    }
  };

  useEffect(() => {
    initPage();
  }, [twit?.Id]);

  return (
    <TwitCard twit={twit}>
      <VStack>
        {twits.map((item) => (
          <Text key={item.Id} style={styles.timePeriod}>
            {formatDateRange(item.Start, item.Finish)}
          </Text>
        ))}
        <View style={styles.divider} />
        <Text bold style={styles.timeTotal}>
          Total: {formattedDate}
        </Text>
      </VStack>
    </TwitCard>
  );
};
