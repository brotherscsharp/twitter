import { StyleSheet } from 'react-native';
import { NavigationContainer, NavigationProp } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { GluestackUIProvider, config } from '@gluestack-ui/themed';
import { Provider } from 'react-redux';
import { HomeScreen } from './screens/HomeScreen';
import { TwittDetails } from './screens/TwittDetails';
import React from 'react';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import { store } from './store';

export const persistor = persistStore(store);
const Stack = createNativeStackNavigator();

export type ScreenNames = ['Home', 'Details']; // type these manually
export type RootStackParamList = Record<ScreenNames[number], undefined>;
export type StackNavigation = NavigationProp<RootStackParamList>;

export default function App() {
  return (
    <NavigationContainer>
      <SafeAreaProvider>
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <GestureHandlerRootView style={{ flex: 1 }}>
              <GluestackUIProvider config={config.theme}>
                <Stack.Navigator>
                  <Stack.Screen name="Home" component={HomeScreen} options={{ title: 'Twitter' }} />
                  <Stack.Screen name="Details" component={TwittDetails} />
                </Stack.Navigator>
              </GluestackUIProvider>
            </GestureHandlerRootView>
          </PersistGate>
        </Provider>
      </SafeAreaProvider>
    </NavigationContainer>
  );
}
