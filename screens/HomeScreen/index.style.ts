import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    backgroundColor: '#fff',
  },
  indicatorContainer: {
    paddingVertical: 5,
    width: '100%',
  },
});
