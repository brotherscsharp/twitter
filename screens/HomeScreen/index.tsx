import React, { useEffect, useRef, useState } from 'react';
import { FlatList, View } from '@gluestack-ui/themed';
import { ActivityIndicator } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { ITwitResponse } from '../../models/responses/ITwitResponse';
import { getTwits } from '../../services/apiService';
import { TwitItemRender } from '../../helpers/TwitItemRenderHelper';

import styles from './index.style';
import { useGetTwitsQuery } from '../../store/services/twitService';

export const HomeScreen = () => {
  // const [twits, setTweets] = useState<ITwitResponse[]>([]);
  // const [page, setPage] = useState(1);
  // const currentPage = useRef(1);

  // const initPage = async () => {
  //   fetchTwits();
  // };

  // useEffect(() => {
  //   initPage();
  // }, []);

  // useEffect(() => {
  //   fetchTwits();
  // }, [page]);

  // const fetchTwits = async () => {
  //   const twitsRes = await getTwits(page);
  //   setTweets(twits.concat(twitsRes));
  // };

  // const fetchMorePosts = async () => {
  //   setPage(page + 1);
  // };

  const [page, setPage] = useState(1);
  const { data, isFetching } = useGetTwitsQuery(page);
  const twits = data?.results ?? [];

  useEffect(() => {
    console.log(`useEffect = ${data}`);
    console.log(`useEffect isFetching = ${isFetching}`);

    return function () {};
  }, [data, isFetching]);

  const fetchMorePosts = async () => {
    if (!isFetching) return;
    setPage(page + 1);
    // currentPage.current += 1;
    // await fetchPosts(currentPage.current);
  };

  const renderFooterLoadingIndicator = () => {
    return (
      <View style={styles.indicatorContainer}>
        <ActivityIndicator size={'small'} />
      </View>
    );
  };

  // console.log(`page = ${page}`);
  // console.log(`data = ${data}`);

  // console.log(twits);
  return (
    <View style={styles.container}>
      {/* {twits.map((p: any, i: number) => (
        <h3 key={i}>{p.Comment}</h3>
      ))} */}
      <FlatList
        data={twits}
        renderItem={TwitItemRender}
        onEndReachedThreshold={0.4}
        onEndReached={fetchMorePosts}
        keyExtractor={(item) => (item as ITwitResponse).Id}
        ListFooterComponent={renderFooterLoadingIndicator}
      />
      {/* <StatusBar style="auto" /> */}
    </View>
  );
};
