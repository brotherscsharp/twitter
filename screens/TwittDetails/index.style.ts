import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    height: '100%',
  },
  indicatorContainer: {
    paddingVertical: 5,
    width: '100%',
  },
  comment: {
    fontSize: 12,
    width: '80%',
  },
});
