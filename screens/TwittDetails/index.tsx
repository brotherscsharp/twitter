import React, { useEffect, useState } from 'react';
import { Avatar, AvatarFallbackText, HStack, Text, VStack, View } from '@gluestack-ui/themed';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useRoute } from '@react-navigation/native';
import styles from './index.style';
import { TwitItemRender } from '../../helpers/TwitItemRenderHelper';
import { ITwitResponse } from '../../models/responses/ITwitResponse';
import { Card } from '../../components/Card';
import { TwitType } from '../../models/Enums';

export const TwittDetails = () => {
  const route = useRoute();
  const [currentTwit, setCurrentTwit] = useState<ITwitResponse>();

  useEffect(() => {
    const { twit } = route.params;
    setCurrentTwit(twit);
  }, [route]);

  return (
    <View style={styles.container}>
      {currentTwit && (
        <VStack>
          <TwitItemRender item={currentTwit} />
          {currentTwit.Type !== TwitType.Text && (
            <Card>
              <HStack space="md">
                <Avatar bgColor="$amber600" size="md" borderRadius="$full">
                  <AvatarFallbackText>User Avatar</AvatarFallbackText>
                </Avatar>
                <Text style={styles.comment}>{currentTwit.Comment}</Text>
              </HStack>
            </Card>
          )}
        </VStack>
      )}
    </View>
  );
};
