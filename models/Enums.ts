export enum TwitType {
  Text = 1,
  Photo = 2,
  Video = 3,
  Timelog = 4,
}
