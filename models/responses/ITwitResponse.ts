import { TwitType } from '../Enums';

export interface ITwitResponse {
  Id: string;
  Created: string;
  Type: TwitType;
  VideoUrl: string;
  VideoThumbnail: string;
  Comment: string;
}
