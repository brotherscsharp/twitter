import { ITwitTime } from '../common/ITwitTime';

export interface IGetTwitTimeResponse {
  message: string;
  data: ITwitTime[];
}
