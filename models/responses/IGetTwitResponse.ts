import { ITwitResponse } from './ITwitResponse';

export interface IGetTwitResponse {
  message: string;
  data: ITwitResponse[];
}
