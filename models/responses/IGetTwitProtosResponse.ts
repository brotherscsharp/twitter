import { ITwitProtos } from '../common/ITwitProtos';

export interface IGetTwitProtosResponse {
  message: string;
  data: ITwitProtos[];
}
