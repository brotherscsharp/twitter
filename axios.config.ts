import axios from 'axios';
// import store from './src/store';

const instance = axios.create({
  baseURL: 'https://interview-fs.ustage.sharp-dev.net/api/',
});

instance.interceptors.request.use(
  (config) => {
    // const { token } = store.getState().user;

    // if (token) {
    //   config.headers.Authorization = `Bearer ${token}`;
    // }

    return config;
  },
  (error) => Promise.reject(error),
);

export default instance;
