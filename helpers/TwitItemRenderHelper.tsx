import React from 'react';
import { TwitPhoto } from '../components/TwitPhoto';
import { TwitVideo } from '../components/TwitVideo';
import { TwitTimeLog } from '../components/TwitTimeLog';
import { TwitText } from '../components/TwitText';
import { ITwitResponse } from '../models/responses/ITwitResponse';

type TwitItemRenderProps = {
  item: ITwitResponse;
};

export const TwitItemRender = ({ item }: TwitItemRenderProps) => {
  switch (item.Type) {
    case 1:
      return <TwitText twit={item} />;
    case 2:
      return <TwitPhoto twit={item} />;
    case 3:
      return <TwitVideo twit={item} />;
    case 4:
      return <TwitTimeLog twit={item} />;
    default:
      return <TwitText twit={item} />;
  }
};
