import moment from 'moment';
import type { FullDate } from '../models/FullDate';

type Period = {
  Id: string;
  Start: string;
  Finish: string;
};

export const calculateTotalTime = (periods: Period[]): number => {
  const totalSeconds = periods.reduce((sum, period) => {
    const startTime: Date = new Date(period.Start);
    const finishTime: Date = new Date(period.Finish);
    const periodDuration = finishTime.getTime() - startTime.getTime();

    return sum + new Date(periodDuration).getTime();
  }, 0);
  return totalSeconds;
};

export const convertMiliseconds = (miliseconds: number): FullDate => {
  const total_seconds = parseInt(Math.floor(miliseconds / 1000).toString());
  const total_minutes = parseInt(Math.floor(total_seconds / 60).toString());
  const total_hours = parseInt(Math.floor(total_minutes / 60).toString());
  const days = parseInt(Math.floor(total_hours / 24).toString());
  const months = parseInt(Math.floor(days / 30).toString());
  const years = parseInt(Math.floor(months / 12).toString());

  const seconds = parseInt((total_seconds % 60).toString());
  const minutes = parseInt((total_minutes % 60).toString());
  const hours = parseInt((total_hours % 24).toString());

  return { years, months, days, hours, minutes, seconds };
};

export const formatDateRange = (dateStart: string, dateEnd: string): string => {
  const start = moment(dateStart).format('MMMM DD YYYY, h:mm');
  const end = moment(dateEnd).format('MMMM DD YYYY, h:mm');

  return `${start} - ${end}`;
};

export const formatTotalDate = (date: FullDate): string => {
  const years = date.years && date.years > 0 ? `${date.years}y ` : '';
  const months = date.months && date.months > 0 ? `${date.months}M ` : '';
  const days = date.days && date.days > 0 ? `${date.days}d ` : '';
  const hours = date.hours && date.hours > 0 ? `${date.hours}h ` : '';
  const minutes = date.minutes && date.minutes > 0 ? `${date.minutes}m` : '';
  return `${years}${months}${days}${hours}${minutes}`;
};
