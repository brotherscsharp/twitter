import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const twitApi = createApi({
  reducerPath: 'twitApi',
  baseQuery: fetchBaseQuery({ baseUrl: 'https://interview-fs.ustage.sharp-dev.net/api/' }),
  endpoints: (builder) => ({
    getTwits: builder.query({
      query: (page) => {
        console.log(`tweets?page=  = ${page}`);
        return `tweets?page=${page}`;
      },
      // Only have one cache entry because the arg always maps to one string
      serializeQueryArgs: ({ endpointName }) => {
        return endpointName;
      },
      // Always merge incoming data to the cache entry
      merge: (currentCache, newItems) => {
        currentCache.results.push(...newItems.results);
      },
      // Refetch when the page arg changes
      forceRefetch({ currentArg, previousArg }) {
        return currentArg !== previousArg;
      },
    }),
  }),
});

export const { useGetTwitsQuery } = twitApi;
