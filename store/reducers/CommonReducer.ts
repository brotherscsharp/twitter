/* eslint-disable */
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { WritableDraft } from 'immer/dist/internal';
import { CommonState } from '../../models/CommonState';

export const INITIAL_STATE: CommonState = {
  likedTwits: [],
};

const commonSlice = createSlice({
  name: 'commonSlice',
  initialState: INITIAL_STATE,
  reducers: {
    setLikedTwits: (state: WritableDraft<CommonState>, action: PayloadAction<string>) => {
      if (state.likedTwits.includes(action.payload)) {
        const filtered = state.likedTwits.filter((item) => item !== action.payload);
        return { ...state, likedTwits: filtered };
      }
      return { ...state, likedTwits: [...state.likedTwits, action.payload] };
    },
  },
});

export const { setLikedTwits } = commonSlice.actions;

export default commonSlice.reducer;
/* eslint-enable */
