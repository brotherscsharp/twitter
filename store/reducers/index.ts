import { combineReducers } from 'redux';
import CommonReducer from './CommonReducer';
import { twitApi } from '../services/twitService';

export default combineReducers({
  common: CommonReducer,
  [twitApi.reducerPath]: twitApi.reducer,
});
